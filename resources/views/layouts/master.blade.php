<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Corona Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('corona-admin/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('corona-admin/assets/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End Plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="{{asset('corona-admin/assets/css/style.css')}}">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{asset('corona-admin/assets/images/favicon.png')}}" />
  @stack('style')
  
</head>

<body>
  
  <div class="container-scroller">
    <!-- partial:../../partials/_sidebar.html -->
    @include('partial.sidebar');
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_navbar.html -->
      @include('partial.navbar');
      <!-- partial -->
      <div class="main-panel">
        @include('sweetalert::alert')
        <div class="content-wrapper">
        <div class="col-14">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        <div class="carousel-item active">
        <img src="https://www.static-src.com/siva/asset//02_2022/HPM-homepage-2000x500-desktop-16feb.jpg?w=960" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
        <img src="https://www.static-src.com/siva/asset//02_2022/rabcan-homepage2000x500-desktop-16feb.jpg?w=960" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
        <img src="https://www.static-src.com/siva/asset//02_2022/intel_core_2000x500px_desktop_16feb.jpg?w=960" class="d-block w-100" alt="...">
        </div>

	</div> 
	</div> 
	</div>  
          @yield('partial.content')
          
        </div>

  

        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com
              2020</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a
                href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">Bootstrap admin
                templates</a> from Bootstrapdash.com</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{asset('corona-admin/assets/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('corona-admin/assets/js/off-canvas.js')}}"></script>
  <script src="{{asset('corona-admin/assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('corona-admin/assets/js/misc.js')}}"></script>
  <script src="{{asset('corona-admin/assets/js/settings.js')}}"></script>
  <script src="{{asset('corona-admin/assets/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page -->
  <!-- End custom js for this page -->
</body>

</html>