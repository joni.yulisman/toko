<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Corona Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('corona-admin/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('corona-admin/assets/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End Plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="{{asset('corona-admin/assets/css/style.css')}}">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{asset('corona-admin/assets/images/favicon.png')}}" />
  @stack('style')
</head>
<body>
<div class="container-scroller">
    <!-- partial:../../partials/_sidebar.html -->
    @include('partial.sidebar');
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_navbar.html -->
      @include('partial.navbar');
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

          @yield('partial.content')
          <form method="POST" action="/ulasan">
  @csrf

<a href="/kategori/create" class="btn btn-primary mb-3">Tambah Ulasan</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">User ID</th>
            <th scope="col">Produk</th>
            <th scope="col">Ulasan</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
<tbody>
    @forelse ($ulasan as $key => $item)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$item->user_id}}</td>
        <td>{{$item->produk_id}}</td>
        <td>{{$item->ulasan}}</td>
        <td>
            
            <form class="mt-2" action="ulasan/{{$item->id}}" method="POST">
            <a href="/ulasan/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/ulasan/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
</form>
        </td>

    </tr>
    @empty
    <h1>Data tidak ada</h1>
    @endforelse
</tbody>
</table>

</body>
</html>