<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Corona Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('corona-admin/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{asset('corona-admin/assets/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End Plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="{{asset('corona-admin/assets/css/style.css')}}">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{asset('corona-admin/assets/images/favicon.png')}}" />
  @stack('style')
</head>
<body>
<div class="container-scroller">
    <!-- partial:../../partials/_sidebar.html -->
    @include('partial.sidebar');
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_navbar.html -->
      @include('partial.navbar');
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

          @yield('partial.content')
          <form method="POST" action="/profile">
  @csrf

<a href="/profile/create" class="btn btn-primary mb-3">Tambah Profile</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama lengkap</th>
            <th scope="col">Umur</th>
            <th scope="col">Nomor Hp</th>
            <th scope="col">Alamat</th>
            <th scope="col">User Id</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
<tbody>
    @forelse ($profile as $key => $item)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$item->nama_lengkap}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->nomor_hp}}</td>
        <td>{{$item->alamat}}</td>
        <td>{{$item->user_id}}</td>
        <td>
            
            <form class="mt-2" action="profile/{{$item->id}}" method="POST">
            <a href="/profile/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/profile/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
</form>
        </td>

    </tr>
    @empty
    <h1>Data tidak ada</h1>
    @endforelse
</tbody>
</table>

</body>
</html>