<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public function produk()
  {
   return $this->hasMany('App\Produk');
  }  
    protected $table = 'kategori';
    protected $fillable = ['nama', 'deskripsi'];
}
