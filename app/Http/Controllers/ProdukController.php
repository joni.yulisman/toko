<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produk/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'kategori_id' => 'required',
            'image' => 'required',
        ],
    [
        'nama.required' => 'Nama harus diisi',
        'deskripsi.required' => 'Deskripsi harus diisi',
        'harga.required' => 'Harga harus diisi',
        'kategori_id.required' => 'Kategori harus diisi',
    ]);

    $produk = new Produk;
    $produk->nama_produk = $request->nama_produk;
    $produk->deskripsi = $request->deskripsi;
    $produk->harga = $request->harga;
    $produk->kategori_id = $request->kategori_id;
    $produk->image = $request->image;
    $produk->save();

    return redirect('/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::find($id)->first();

        return view('produk.show', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);

        return view('produk.edit', compact('produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'kategori_id' => 'required',
            'image' => 'required',
        ],
    [
        'nama_produk.required' => 'Nama harus diisi',
        'deskripsi.required' => 'Deskripsi harus diisi',
        'harga.required' => 'Deskripsi harus diisi',
        'kategori_id.required' => 'Kategori harus diisi',
    ]);

    $produk = Produk::find($id);

    $produk->nama_produk = $request['nama_produk'];
    $produk->deskripsi = $request['deskripsi'];
    $produk->harga = $request['nama_produk'];
    $produk->kategori_id = $request['kategori_id'];
    $produk->image = $request['image'];
    $produk->save();

    return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $produk->delete();
        return redirect('/produk');
    }
}
