<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ulasan;

class UlasanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ulasan = Ulasan::all();
        return view('ulasan.index', compact('ulasan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ulasan/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'produk_id' => 'required',
            'ulasan' => 'required',
        ],
    [
        'user_id.required' => 'Usrer harus diisi',
        'produk_id.required' => 'Produk harus diisi',
        'ulasan.required' => 'Ulasan harus diisi',
    ]);

    $ulasan = new Ulasan;
    $ulasan->user_id = $request->user_id;
    $ulasan->produk_id = $request->produk_id;
    $ulasan->ulasan = $request->ulasan;
    $ulasan->save();

    return redirect('/ulasan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ulasan = Ulasan::find($id)->first();

        return view('ulasan.show', compact('ulasan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ulasan = Ulasan::find($id);

        return view('ulasan.edit', compact('ulasan'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'produk_id' => 'required',
            'ulasan' => 'required',
        ],
    [
        'user_id.required' => 'User harus diisi',
        'produk_id.required' => 'Produk harus diisi',
        'ulasan.required' => 'Ulasan harus diisi',
    ]);

    $ulasan = Ulasan::find($id);

    $ulasan->user_id = $request['user_id'];
    $ulasan->produk_id = $request['produk_id'];
    $ulasan->ulasan = $request['ulasan'];

    $ulasan->save();

    return redirect('/ulasan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ulasan = Ulasan::find($id);
        $ulasan->delete();
        return redirect('/ulasan'); 
    }
}
