<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use RealRashid\SweetAlert\Facades\Alert;


class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ],
    [
        'nama.required' => 'Nama harus diisi',
        'deskripsi.required' => 'Deskripsi harus diisi',
    ]);

    $kategori = new Kategori;
    $kategori->nama = $request->nama;
    $kategori->deskripsi = $request->deskripsi;

    $kategori->save();

    Alert::success('Berhasil', 'Tambah Kategori Berhasil');
    return redirect('/kategori');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::find($id)->first();

        return view('kategori.show', compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);

        return view('kategori.edit', compact('kategori'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ],
    [
        'nama.required' => 'Nama harus diisi',
        'deskripsi.required' => 'Deskripsi harus diisi',
    ]);

    $kategori = Kategori::find($id);

    $kategori->nama = $request['nama'];
    $kategori->deskripsi = $request['deskripsi'];

    $kategori->save();

    Alert::success('Berhasil', 'Update Kategori Berhasil');
    return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        $kategori->delete();
        Alert::success('Berhasil', 'Hapus Kategori Berhasil');
        return redirect('/kategori'); 
    }
}
