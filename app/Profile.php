<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    protected $table = 'profile';
    protected $fillable = ['nama_lengkap', 'umur','nomor_hp', 'alamat', 'user_id'];
}
