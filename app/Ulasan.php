<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    public function user()
    {
     return $this->belongsToMany('App\User');
    }  
    public function produk()
    {
     return $this->belongsToMany('App\Produk');
    }
    protected $table = 'ulasan';
    protected $fillable = ['user_id', 'produk_id', 'ulasan'];
}
