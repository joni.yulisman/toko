<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    function upload(Request $request)
    {
        $file = $request->file('image');
   
      // Mendapatkan Nama File
      $nama_file = $file->getClientOriginalName();
   
      // Mendapatkan Extension File
      $extension = $file->getClientOriginalExtension();
  
      // Mendapatkan Ukuran File
      $ukuran_file = $file->getSize();
   
      // Proses Upload File
      $destinationPath = 'uploads';
      $file->move($destinationPath,$file->getClientOriginalName());
    }
    public function kategori()
  {
    return $this->belongsTo('App\Kategori');
  }


    protected $table = 'produk';
    protected $fillable = ['nama_produk', 'deskripsi', 'harga', 'kategori_id', 'image'];
}
