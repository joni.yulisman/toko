<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('welcome');});
Route::get('/', function() {
    return view('layouts.master');
});



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');



//CRUD Kategori
//Create
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');

//Read
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');

//Update
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');

//Delete
Route::delete('kategori/{kategori_id}', 'KategoriController@destroy');

//CRUD Produk
//Create
Route::get('/produk/create', 'ProdukController@create');
Route::post('/produk', 'ProdukController@store');

//Read
Route::get('/produk', 'ProdukController@index');
Route::get('/produk/{produk_id}', 'ProdukController@show');

//Update
Route::get('/produk/{produk_id}/edit', 'ProdukController@edit');
Route::put('/produk/{produk_id}', 'ProdukController@update');

//Delete
Route::delete('produk/{produk_id}', 'ProdukController@destroy');

//CRUD Profile
//Create
Route::get('/profile/create', 'ProfileController@create');
Route::post('/profile', 'ProfileController@store');

//Read
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/{profile_id}', 'ProfileController@show');

//Update
Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');
Route::put('/profile/{profile_id}', 'ProfileController@update');

//Delete
Route::delete('profile/{profile_id}', 'ProfileController@destroy');

//CRUD Ulasan
//Create
Route::get('/ulasan/create', 'UlasanController@create');
Route::post('/ulasan', 'UlasanController@store');

//Read
Route::get('/ulasan', 'UlasanController@index');
Route::get('/ulasan/{ulasan_id}', 'UlasanController@show');

//Update
Route::get('/ulasan/{ulasan_id}/edit', 'UlasanController@edit');
Route::put('/ulasan/{ulasan_id}', 'UlasanController@update');

//Delete
Route::delete('ulasan/{ulasan_id}', 'UlasanController@destroy');

// Route untuk menampilkan form upload
Route::get('form-upload','UploadController@form');

// Route untuk menghandle proses upload
Route::post('upload','UploadController@upload');